import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import moment from "moment";
import delay from "lodash/delay";
import "./style.css";
import LoalTimer from "./LoalTimer";

class Converter extends React.Component {
  state = {
    date: "",
    year: "",
    month: "",
    secs: "",
    minute: "",
    hour: "",
    timeStamps: ""
  };

  handleReset = () => {
    const now = moment();
    const year = now.local().format("YYYY");
    const date = now.local().format("DD");
    const month = now.local().format("MM");
    const hour = now.local().format("HH");
    const minute = now.local().format("mm");
    const secs = now.local().format("ss");
    const timeStamps = now.unix().valueOf();
    this.setState({ timeStamps, date, year, month, secs, hour, minute });
  };

  handleChange = (event, type, limit, maxValue) => {
    const numbers = /^[0-9]+$|^$|^\s$/;
    const match = event.target.value.match(numbers);
    const eventValue = event.target.value.trim();

    if (eventValue.length <= limit && eventValue <= maxValue && match) {
      this.setState({
        [type]: eventValue
      });

      delay(this.convertTiming, 1000);
    }
  };

  convertTiming = () => {
    const { year, date, month, secs, minute, hour } = this.state;
    if (!year && !date && !month && !minute && !hour) {
      return this.setState({
        timeStamps: ""
      });
    }
    const value = moment()
      .year(year)
      .month(month)
      .date(date)
      .hour(hour)
      .minute(minute)
      .seconds(secs);
    const timeStamps = value.unix();
    this.setState({
      timeStamps
    });
  };

  render() {
    const { year, date, month, secs, minute, hour, timeStamps } = this.state;

    return (
      <main className="local-container">
        <section className="timer-block">
          <LoalTimer />
        </section>
        <section className="converter-wrapper local">
          <div className="input-field-block year">
            <span>Convert</span>
            <TextField
              id="year"
              label="YYYY"
              type="text"
              value={year}
              onChange={event => this.handleChange(event, "year", 6, 999999)}
              className="input-field-border"
            />
            <TextField
              id="month"
              label="MM"
              type="text"
              value={month}
              onChange={event => this.handleChange(event, "month", 2, 12)}
              className="input-field-border"
            />
            <TextField
              id="date"
              label="DD"
              type="text"
              value={date}
              onChange={event => this.handleChange(event, "date", 2, 31)}
              className="input-field-border"
            />
          </div>
          <div className="input-field-block time">
            <span>and</span>
            <TextField
              id="year"
              label="HH"
              type="text"
              value={hour}
              onChange={event => this.handleChange(event, "hour", 2, 23)}
              className="input-field-border"
            />
            <TextField
              id="year"
              label="MM"
              type="text"
              value={minute}
              onChange={event => this.handleChange(event, "minute", 2, 60)}
              className="input-field-border"
            />
            <TextField
              id="year"
              label="SS"
              type="text"
              value={secs}
              onChange={event => this.handleChange(event, "secs", 2, 60)}
              className="input-field-border"
            />
          </div>

          <div className="input-field-block readOnly">
            <TextField
              InputProps={{
                readOnly: true
              }}
              id="timeStamps"
              label="to seconds since epoch"
              type="text"
              value={timeStamps}
              className="input-field"
            />
          </div>

          <footer>
            <Button
              variant="contained"
              onClick={this.handleReset}
              color="secondary"
            >
                Now
            </Button>
          </footer>
        </section>
      </main>
    );
  }
}

export default Converter;
