import React from 'react';
import moment from 'moment';

class Countdown extends React.Component {
    state = {
        date: undefined,
        time: undefined,
    };

    componentDidMount() {
        this.interval = setInterval(() => {
            const now = moment();
            const date = now.local().format("D MMMM YYYY");
            const time=now.local().format('HH:mm:ss'); 
            this.setState({ time,date });
        }, 1000);
    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    render() {
        const { date, time} = this.state;

        return (
            <div>
                <div className="countdown-wrapper">
                    <div className="countdown-item">
                    <h3>local date</h3>
                        {date}
                    </div>
                    <div className="countdown-item">
                    <h3>local time</h3>
                        {time}
                    </div>
                    <div className="hint">
                        <p>local timezone(UTC+5.5h)</p>
                        <h3>india Standard</h3>
                    </div>
                </div>
            </div>
        );
    }
}

export default Countdown;