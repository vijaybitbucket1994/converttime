import React from 'react';
import moment from 'moment';

class Countdown extends React.Component {
    state = {
        date: undefined,
        time: undefined,
        timeStamps: undefined
    };

    componentDidMount() {
        this.interval = setInterval(() => {
            const now = moment();
            const date = now.utc().format("D MMMM YYYY");
            const time=now.utc().format('HH:mm:ss'); 
            const timeStamps = now.unix().valueOf();

            this.setState({ timeStamps,time,date });
        }, 1000);
    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    render() {
        const { date ,timeStamps,time} = this.state;

        return (
            <div>
                <div className="countdown-wrapper">
                    <div className="countdown-item">
                    <h3>UTC date</h3>
                        {date}
                    </div>
                    <div className="countdown-item">
                    <h3>UTC time</h3>
                        {time}
                    </div>
                    <div className="countdown-item">
                    <h3>UNIX time</h3>
                        {timeStamps}
                    </div>
                </div>
            </div>
        );
    }
}

export default Countdown;