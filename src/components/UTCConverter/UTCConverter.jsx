import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import moment from "moment";
import delay from "lodash/delay";
import "./style.css";
import Timer from "./Timer";

class Converter extends React.Component {
  state = {
    timeStamps: "",
    UTCdate: "",
    Localdate: ""
  };

  handleReset = () => {
    const now = moment();
    const timeStamps = now.unix().valueOf();
    let date = moment.utc().format("dddd MMMM DD YYYY HH:mm:ss");
    let local = moment()
      .local()
      .format("dddd MMMM DD YYY HH:mm:ss");
    this.setState({ timeStamps, UTCdate: date, Localdate: local });
  };

  handleChangeSecs = event => {
    const numbers = /^[0-9]+$|^$|^\s$/;
    const match = event.target.value.match(numbers);
    let eventValue = event.target.value.trim();
    if (match) {
      this.setState({
        timeStamps: eventValue
      });
      delay(this.convertTiming, 1000, eventValue);
    }
  };

  convertTiming = val => {
    if (!this.state.timeStamps) {
      return this.setState({
        UTCdate: "",
        Localdate:""
      });
    }

    let newDate = moment.unix(val).format();
    let date = moment(newDate).utc().format("dddd MMMM DD YYYY HH:mm:ss");
    let stillUtc = moment.utc(newDate).toDate();
    let local = moment(stillUtc)
      .local()
      .format("dddd MMMM DD YYYY HH:mm:ss");
    this.setState({
      UTCdate: date,
      Localdate: local
    });
  };

  render() {
    const { timeStamps, UTCdate, Localdate } = this.state;

    return (
      <main className="utc-container">
        <section className="timer-block">
          <Timer />
        </section>
        <section className="converter-wrapper utc">
          <div className="input-field-block secs">
            <TextField
              id="time"
              label="Convert seconds"
              type="text"
              value={timeStamps}
              onChange={this.handleChangeSecs}
              className="input-field"
            />
          </div>
          <div className="input-field-block date readOnly">
            <TextField
              InputProps={{
                readOnly: true
              }}
              id="time"
              label="to UTC time & date"
              value={UTCdate}
              className="input-field"
            />
          </div>
          <div className="input-field-block localdate readOnly">
            <TextField
              InputProps={{
                readOnly: true
              }}
              id="time"
              label="to local time & date"
              type="text"
              value={Localdate}
              className="input-field"
              size="large"
            />
          </div>
          <footer>
            <Button
              variant="contained"
              onClick={this.handleReset}
              color="secondary"
            >
              Now
            </Button>
          </footer>
        </section>
      </main>
    );
  }
}

export default Converter;
