import React from 'react';
import './App.css';
import UTCConverter from './components/UTCConverter/UTCConverter';
import LocalConverter from './components/LocalConverter/LocalConverter';

function App() {
       
  return (
    <div className="App">
      <main className="container">
          <UTCConverter/>
          <LocalConverter/>
      </main>
    </div>
  );
}

export default App;
